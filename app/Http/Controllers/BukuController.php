<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BukuModel as Buku;
use App\Models\KategoriBukuModel as KategoriBuku;
use App\Models\TransaksiBukuModel as TransaksiBuku;

class BukuController extends Controller
{
	public $buku;
	public $kategori_buku;
    public $transaksi_buku;

    public function __construct(Buku $buku,KategoriBuku $kategori_buku,TransaksiBuku $transaksi_buku)
    {
    	$this->buku = $buku;
    	$this->kategori_buku = $kategori_buku;
        $this->transasksi_buku = $transaksi_buku;
    }

    public function TambahBuku(Request $request)
    {
		$foto_buku = $request->foto_buku;
		$fileName  = date('Y-m-d').'_'.$foto_buku->getClientOriginalName();
		$foto_buku->move(public_path('foto_buku'),$fileName);
    	$data_buku = [
			'judul_buku'       => $request->judul_buku,
			'penerbit'         => $request->penerbit,
			'tahun_terbit'     => $request->tahun_terbit,
			'id_kategori_buku' => $request->kategori_buku,
			'stok_buku'        => $request->stok,
			'foto_buku'        => $fileName,
			'created_at'       => date('Y-m-d H:i:s'),
			'updated_at'       => date('Y-m-d H:i:s'),
    	];
    	$this->buku->create($data_buku);
    	if ($request->segment(2)=="petugas") {
			return redirect('/petugas/data-buku')->with('success','Buku Telah Terinput');
    	}
    	else if ($request->segment(2)=="admin") {
			return redirect('/admin/data-buku')->with('success','Buku Telah Terinput');
    	}
    }

    public function UpdateBuku($id_buku,Request $request)
    {
		if ($request->foto_buku!='') {	
			$foto_buku = $request->foto_buku;
			$fileName  = date('Y-m-d').'_'.$foto_buku->getClientOriginalName();
			$foto_buku->move(public_path('foto_buku'),$fileName);
	    	$data_buku = [
				'judul_buku'       => $request->judul_buku,
				'penerbit'         => $request->penerbit,
				'tahun_terbit'     => $request->tahun_terbit,
				'id_kategori_buku' => $request->kategori_buku,
				'stok_buku'        => $request->stok,
				'foto_buku'        => $fileName,
				'created_at'       => date('Y-m-d H:i:s'),
				'updated_at'       => date('Y-m-d H:i:s'),
	    	];
		}
		else {
			$data_buku = [
				'judul_buku'       => $request->judul_buku,
				'penerbit'         => $request->penerbit,
				'tahun_terbit'     => $request->tahun_terbit,
				'id_kategori_buku' => $request->kategori_buku,
				'stok_buku'        => $request->stok,
				'created_at'       => date('Y-m-d H:i:s'),
				'updated_at'       => date('Y-m-d H:i:s'),
	    	];
		}
    	$this->buku->where('id_buku',$id_buku)->update($data_buku);
    	if ($request->segment(2)=="petugas") {
			return redirect('/petugas/data-buku')->with('success','Buku Telah Terupdate');
    	}
    	else if ($request->segment(2)=="admin") {
			return redirect('/admin/data-buku')->with('success','Buku Telah Terupdate');
    	}
    }

    public function DeleteBuku($id_buku)
    {
    	$this->buku->where('id_buku',$id_buku)->delete();
    	if ($request->segment(2)=="petugas") {
    		return redirect('/petugas/data-buku');
    	}
    	else if ($request->segment(2)=="admin") {
    		return redirect('/admin/data-buku');
    	}
    }

    public function TambahKategoriBuku(Request $request)
    {
		$foto_buku = $request->foto_buku;
		$fileName  = date('Y-m-d').'_'.$foto_buku->getClientOriginalName();
		$foto_buku->move(public_path('foto_buku'),$fileName);
    	$data_kategori_buku = [
			'nama_kategori'      => $request->nama_kategori,
			'deskripsi_kategori' => $request->deskripsi
    	];
    	$this->kategori_buku->create($data_kategori_buku);
    	if ($request->segment(2)=="petugas") {
			return redirect('/petugas/data-kategori-buku')->with('success','Buku Telah Terinput');
    	}
    	else if ($request->segment(2)=="admin") {
			return redirect('/admin/data-kategori-buku')->with('success','Buku Telah Terinput');
    	}
    }

    public function UpdateKategoriBuku($id_kategori_buku,Request $request)
    {
		$data_kategori_buku = [
			'nama_kategori'      => $request->nama_kategori,
			'deskripsi_kategori' => $request->deskripsi
    	];
    	$this->buku->where('id_buku',$id_buku)->update($data_kategori_buku);
    	if ($request->segment(2)=="petugas") {
			return redirect('/petugas/data-kategori-buku')->with('success','Kategori Buku Telah Terupdate');
    	}
    	else if ($request->segment(2)=="admin") {
			return redirect('/admin/data-kategori-buku')->with('success','Kategori Buku Telah Terupdate');
    	}
    }

    public function DeleteKategoriBuku($id_kategori_buku,Request $request)
    {
    	$this->buku->where('id_kategori_buku',$id_kategori_buku)->delete();
    	if ($request->segment(2)=="petugas") {
    		return redirect('/petugas/data-kategori-buku');
    	}
    	else if ($request->segment(2)=="admin") {
    		return redirect('/admin/data-kategori-buku');
    	}
    }

    public function PinjamBuku(Request $request) 
    {
        $data_pinjam = [
            'id_buku' => $request->buku,
            'id_siswa' => $request->siswa,
            'tanggal_pinjam_buku' => $request->tanggal_pinjam,
            'tanggal_jatuh_tempo' => $request->tanggal_jatuh_tempo,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $this->transasksi_buku->create($data_pinjam);
        if ($request->segment(2)=="petugas") {
            return redirect('/petugas/data-peminjaman');
        }
        else if ($request->segment(2)=="admin") {
            return redirect('/admin/data-peminjaman');
        }
    }

    public function KembalikanBuku($id_transaksi,Request $request)
    {
        $tanggal_pinjam = $this->transaksi_buku->select('tanggal_pinjam')->where('id_transaksi',$id_transaksi)->first();
        $tanggal_kembali = $request->tanggal_kembali;
        $denda = HitungDenda($tanggal_pinjam,$tanggal_kembali);
        $data_kembali = [
            'tanggal_kembalikan_buku' => $tanggal_kembali,
            'status' => $request->status,
            'denda' => $denda,
            'updated_at' => date('Y-m-d H:i:s')
        ];
        $this->transaksi_buku->where('id_transaksi',$id_transaksi)->update($data_kembali);
        if ($request->segment(2)=="petugas") {
            return redirect('/petugas/data-kembali-buku');
        }
        else if ($request->segment(2)=="admin") {
            return redirect('/admin/data-kembali-buku');
        }
    }

    public function HitungDenda($tgl1,$tgl2)
    {
        // TODO:Hitung Seluruh Hari yang merupakan kelipatan 7
    }
}
