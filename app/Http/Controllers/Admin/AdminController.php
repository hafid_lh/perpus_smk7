<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Siswa\SiswaModel as Siswa;
use App\Models\Petugas\PetugasModel as Petugas;
use App\Models\BukuModel as Buku;

class AdminController extends Controller
{
    public function __construct(Siswa $siswa, Petugas $petugas, Buku $buku)
    {
		$this->siswa   = $siswa;
		$this->petugas = $petugas;
		$this->buku    = $buku;
    }
    // public function 
}
