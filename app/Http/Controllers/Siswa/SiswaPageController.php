<?php

namespace App\Http\Controllers\Siswa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Siswa\SiswaModel as Siswa;
use Auth;

class SiswaPageController extends Controller
{
    public function Dashboard()
    {
    	$get = Siswa::where('username',Auth::user()->username)->firstOrFail();
    	return view('Siswa.page.dashboard',compact('get'));
    }

	public function LoginForm()
	{
		return view('Siswa.page.login-form');
	}

	public function PageSiswa()
	{
		return view('Siswa.page.dashboard');
	}

	public function ProfileSiswa($username)
	{
		$get = Siswa::where('username',$username)->firstOrFail();
		return view('Siswa.page.profile',compact('get'));
	}

	Public function EditProfile($username)
	{
		$get = Siswa::where('username',$username)->firstOrFail();
		return view('Siswa.page.edit-profile',compact('get'));
	}
}
