<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransaksiBukuModel extends Model
{
    protected $table = 'transaksi_buku';
    protected $guarded = [];
    protected $primaryKey = 'id_transaksi';
}
