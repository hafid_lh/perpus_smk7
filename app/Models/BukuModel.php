<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Models\KategoriBukuModel as KategoriBuku;

class BukuModel extends Model
{
    protected $table = 'buku';
    protected $guarded = [];
    protected $primaryKey = 'id_buku';
    public function kategori()
    {
    	return $this->belongsTo('KategoriBuku','id_kategori_buku');
    }
}
