<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')| Perpus SMKN 7 Samarinda</title>
	<style>
		img {
			width:100px;
			height:100px;
			border-radius:20em;
		}
	</style>
</head>
<body>
	@include('Siswa.layout.header')
	@yield('content')
	@include('Siswa.layout.footer')
	@yield('js-script')
</body>
</html>
