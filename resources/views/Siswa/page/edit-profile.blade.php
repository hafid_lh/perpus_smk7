@extends('Siswa.layout.layout-app')
@section('title') Edit @endsection
@section('content')
	<form action="{{ url('/edit/profile/siswa',Auth::user()->username) }}" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="text" name="username" value="{{ $get->username }}" placeholder="Username">
		<input type="password" name="password" placeholder="Password">
		<input type="text" name="nama_siswa" value="{{ $get->nama_siswa }}" placeholder="Nama Lengkap">
		<input type="text" name="nisn" value="{{ $get->nisn }}" placeholder="NISN">
		<select name="jenis_kelamin">
		@if ($get->jenis_kelamin=="Laki-Laki")
			<option value="{{ $get->jenis_kelamin }}">{{ $get->jenis_kelamin }}</option>
			<option value="Perempuan">Perempuan</option>
		@else
			<option value="{{ $get->jenis_kelamin }}">{{ $get->jenis_kelamin }}</option>
			<option value="Laki-Laki">Laki - Laki</option>
		@endif
		</select>
		<input type="text" name="kelas" value="{{ $get->kelas }}" placeholder="Kelas">
		@if ($get->foto_profile!='')
			<img src="{{ asset('profile_siswa/'.$get->foto_profile) }}" alt="">
			<input type="file" name="foto_profile">
		@else
			<input type="file" name="foto_profile">
		@endif
		<button type="submit">Edit</button>
	</form>
@endsection