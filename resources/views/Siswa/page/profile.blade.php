@extends('Siswa.layout.layout-app')
@section('title') Profile @endsection
@section('content')
	<table border="1" width="50%">
		<thead>
			<th>Nama</th>
			<th>Username</th>
			<th>Jenis Kelamin</th>
			<th>NISN</th>
			<th>Kelas</th>
		</thead>
		<tbody>
			<tr align="center">
				<td>{{ $get->nama_siswa }}</td>
				<td>{{ $get->username }}</td>
				<td>{{ $get->jenis_kelamin}}</td>
				<td>{{ $get->nisn }}</td>
				<td>{{ $get->kelas }}</td>
			</tr>
		</tbody>
	</table>
	<a href="{{ url('/edit-profile-siswa',Auth::user()->username) }}">
		Edit Profile
	</a>
@endsection