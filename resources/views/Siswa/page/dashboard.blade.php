@extends('Siswa.layout.layout-app')
@section('title') Dashboard @endsection
@section('content')
	<h1>WELCOME {{ $get->nama_siswa }}</h1>
	<img src="{{ asset('profile_siswa/'.$get->foto_profile) }}" alt="">
	<ul>
		<li><a href="{{ url('/data-buku') }}">Data Buku</a></li>
		<li><a href="{{ url('/data-kategori-buku') }}">Data Kategori Buku</a></li>
		<li><a href="{{ url('/data-peminjaman') }}">Data Peminjaman</a></li>
		<li><a href="{{ url('/data-pengembalian') }}">Data Pengembalian</a></li>
	</ul>
	<h4><a href="{{ url('/profile-siswa',Auth::user()->username) }}">Profile Siswa</a></h4>
	<a href="{{ url('/logout') }}">Keluar?</a>
@endsection