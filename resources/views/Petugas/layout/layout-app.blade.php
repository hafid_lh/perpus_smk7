<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<style>
		li {
			margin-top:10px;
			font-size:20px;
		}
		img {
			border-radius: 20em;
			width:100px;
			height:100px;
		}
	</style>
</head>
<body>
	@yield('content')
</body>
</html>