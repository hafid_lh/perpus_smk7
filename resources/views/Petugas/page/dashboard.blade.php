@extends('Petugas.layout.layout-app')
@section('title') Petugas @endsection
@section('content')
	@if ($petugas->nama_petugas==null && $petugas->nip==null && $petugas->jenis_kelamin==null)
		<h1>Silahkan Lengkapi Profile Anda Terlebih Dahulu <a href="{{ url('/profile-petugas') }}">Disini</a></h1>
	@else
		<h1>Selamat Datang {{ Auth::user()->username }}</h1>
		<img src="{{ asset('petugas_profile/'.$petugas->foto_profile) }}" alt="">
		<ul>
			<li><a href="{{ url('/data-user') }}">Data Siswa</a></li>
			<li><a href="{{ url('/data-buku') }}">Data Buku</a></li>
			<li><a href="{{ url('/data-kategori-buku') }}">Data Kategori Buku</a></li>
			<li><a href="{{ url('/data-peminjaman') }}">Data Peminjaman</a></li>
			<li><a href="{{ url('/data-pengembalian') }}">Data Pengembalian</a></li>
		</ul>
	@endif
	<h4><a href="{{ url('/logout') }}">Logout</a></h4>
@endsection