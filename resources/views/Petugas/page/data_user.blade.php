@extends('Siswa.layout.layout-app')
@section('title') Data User @endsection
@section('content')
@if (Session::has('sukses'))
	<h3>{{ session('sukses') }}</h3>
@endif
<a href="{{ url('/dashboard-petugas') }}">
	Kembali
</a>
	<table border="1" width="60%">
		<thead>
			<th>No</th>
			<th>Username</th>
			<th>Nama Siswa</th>
			<th>Nisn</th>
			<th>Jenis Kelamin</th>
			<th>Kelas</th>
			<th>Action</th>
		</thead>
		<tbody>
		@foreach ($data as $no => $siswa)
			<tr align="center">
				<td>{{ $no+1 }}</td>
				<td>{{ $siswa->username }}</td>
				<td>{{ $siswa->nama_siswa }}</td>
				<td>{{ $siswa->nisn }}</td>
				<td>{{ $siswa->jenis_kelamin }}</td>
				<td>{{ $siswa->kelas }}</td>
				<td>
					<a href="{{ url('/delete/petugas',$siswa->username) }}">
						<button>
							Hapus Data
						</button>
					</a>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
@endsection