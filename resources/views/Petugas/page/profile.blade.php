@extends('Petugas.layout.layout-app')
@section('title') Profile Petugas @endsection
@section('content')
	<form action="{{ url('/update/petugas') }}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
		<input type="text" name="nama_petugas" placeholder="Nama Lengkap">
		<input type="text" name="nip" placeholder="NIP">
		<select name="jenis_kelamin">
			<option value="" disabled selected>Jenis Kelamin</option>
			<option value="Laki-Laki">Laki - Laki</option>
			<option value="Perempuan">Perempuan</option>
		</select>
		<input type="file" name="foto_profile">
		<button type="submit">Perbarui Profile</button>
	</form>
@endsection