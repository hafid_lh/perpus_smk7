<!DOCTYPE html>
<html lang="id">
<head>
	<meta charset="UTF-8">
	<title>Login Form</title>
</head>
<body>
@if (count($errors) > 0)
@foreach ($errors->all() as $error)
	{{ $error }}
@endforeach
@endif
	<form action="{{ url('/login/auth') }}" method="POST">
	{{ csrf_field() }}
		<input type="text" name="username" placeholder="Username">
		<input type="password" name="password" placeholder="Password">
		<button type="submit">Login</button>
	</form>
</body>
</html>