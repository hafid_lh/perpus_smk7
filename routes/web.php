<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){
	return view('welcome');
});
Route::group(['middleware'=>'isAuth'],function (){	

//----------Auth----------//
	Route::get('/login-form',['uses'=>'AuthController@FormLogin','as'=>'form-login']);
	Route::post('/login/auth',['uses'=>'AuthController@Authenticate','as'=>'form-post-login']);
	Route::get('/logout',['uses'=>'AuthController@AuthLogout','as'=>'logout']);
//-------------------------------//

//----------Route Siswa----------//
	Route::group(['middleware'=>'hasSiswa'],function(){
		Route::get('/dashboard-siswa',['uses'=>'Siswa\SiswaPageController@Dashboard','as'=>'dashboard-siswa-page']);
		Route::get('/profile-siswa/{username}',['uses'=>'Siswa\SiswaPageController@ProfileSiswa','as'=>'profile-siswa-page']);
		Route::get('/edit-profile-siswa/{username}',['uses'=>'Siswa\SiswaPageController@EditProfile','as'=>'edit-profile-siswa']);
		Route::post('/edit/profile/siswa/{username}',['uses'=>'Siswa\SiswaController@UpdateProfile','as'=>'post-updated-profile-siswa']);
	});
//----------End Siswa----------//

//----------Route Petugas----------//
	// Route::get('/coba','PetugasController@RegisterPetugas');
	Route::group(['middleware'=>'hasPetugas'],function(){
		Route::get('/dashboard-petugas',['uses'=>'Petugas\PetugasPageController@DashboardPetugas','as'=>'petugas-dashboard']);
		// UPDATE PROFILE PETUGAS //
		Route::get('/profile/{username}',['uses'=>'Petugas\PetugasPageController@ProfilePetugas','as'=>'petugas-profile-page']);
		Route::post('/update/petugas',['uses'=>'Petugas\PetugasController@UpdateProfile','as'=>'petugas-update-profile']);
		// END UPDATE PROFILE PETUGAS //

		// Route::get('/data-user',['uses'=>'Petugas\PetugasPageController@DataUser','as'=>'data-user-petugas']);
		// Route::delete('/delete/petugas/{username}',['uses'=>'Petugas\PetugasController@DeleteUser','as'=>'delete-petugas-siswa']);

		// CRUD BUKU //
		Route::get('/petugas/data-buku',['uses'=>'BukuPageController@ShowBuku','as'=>'show-data-buku']);
		Route::post('/insert/petugas/data-buku',['uses'=>'BukuController@TambahBuku','as'=>'insert-data-buku']);
		Route::get('/petugas/edit-buku/{id_buku}',['uses'=>'BukuPageController@EditBuku','as'=>'edit-data-buku']);
		Route::post('/update/petugas/data-buku/{id_buku}',['uses'=>'BukuController@UpdateBuku','as'=>'insert-data-buku']);
		Route::delete('/delete/petugas/data-buku/{id_buku}',['uses'=>'BukuController@DeleteBuku','as'=>'delete-data-buku']);
		// END CRUD BUKU //

		// CRUD KATEGORI BUKU //
		Route::get('/petugas/data-kategori-buku',['uses'=>'BukuPageController@ShowKategoriBuku','as'=>'show-data-buku']);
		Route::post('/insert/petugas/data-kategori-buku',['uses'=>'BukuController@TambahKategoriBuku','as'=>'insert-data-buku']);
		Route::get('/petugas/edit-kategori-buku/{id_kategori_buku}',['uses'=>'BukuPageController@EditKategoriBuku','as'=>'edit-data-buku']);
		Route::post('/update/petugas/data-kategori-buku/{id_kategori_buku}',['uses'=>'BukuController@UpdateKategoriBuku','as'=>'insert-data-buku']);
		Route::delete('/delete/petugas/data-kategori-buku/{id_kategori_buku}',['uses'=>'BukuController@DeleteKategoriBuku','as'=>'delete-data-buku']);
		// END CRUD KATEGORI BUKU //

		// CRUD PEMINJAMAN BUKU //
		Route::get('/petugas/data-peminjaman',['uses'=>'BukuPageController@ShowPinjam','as'=>'page-pinjam-buku']);
		Route::post('/insert/petugas/data-peminjaman',['uses'=>'BukuController@PinjamBuku','as'=>'insert-pinjam-buku']);
		Route::get('/petugas/edit-data-peminjaman/{id_transaksi}',['uses'=>'BukuPageController@EditPinjam','as'=>'edit-pinjam-buku']);
		Route::post('/edit/petugas/data-peminjaman/{id_transaksi}',['uses'=>'BukuController@UpdatePinjam','as'=>'update-pinjam-buku']);
		Route::delete('/delete/petugas/data-pinjam/{id_transaksi}',['uses'=>'BukuController@DeletePinjam','as'=>'delete-pinjam-buku']);
		// END CRUD PEMINJAMAN BUKU //

		// CRUD PENGEMBALIAN BUKU //

		// END CRUD PENGEMBALIAN BUKU //
	});
//----------End Petugas--------//

//----------Route Admin----------//
	Route::group(['middleware'=>'hasAdmin'],function(){
		Route::get('/dashboard-admin',['uses'=>'AdminPageController@DashboardAdmin','as'=>'dashboard-admin']);
		// CRUD BUKU //
		Route::get('/data-buku',['uses'=>'BukuPageController@ShowBuku','as'=>'show-data-buku']);
		Route::post('/insert/admin/data-buku',['uses'=>'BukuController@TambahBuku','as'=>'insert-data-buku']);
		Route::get('/edit-buku/{id_buku}',['uses'=>'BukuPageController@EditBuku','as'=>'edit-data-buku']);
		Route::post('/update/admin/data-buku/{id_buku}',['uses'=>'BukuController@UpdateBuku','as'=>'insert-data-buku']);
		Route::delete('/delete/admin/data-buku/{id_buku}',['uses'=>'BukuController@DeleteBuku','as'=>'delete-data-buku']);
		// END CRUD BUKU //

		// CRUD KATEGORI BUKU //
		Route::get('/data-kategori-buku',['uses'=>'BukuPageController@ShowKategoriBuku','as'=>'show-data-buku']);
		Route::post('/insert/admin/data-kategori-buku',['uses'=>'BukuController@TambahKategoriBuku','as'=>'insert-data-buku']);
		Route::get('/edit-kategori-buku/{id_kategori_buku}',['uses'=>'BukuPageController@EditKategoriBuku','as'=>'edit-data-buku']);
		Route::post('/update/admin/data-kategori-buku/{id_kategori_buku}',['uses'=>'BukuController@UpdateKategoriBuku','as'=>'insert-data-buku']);
		Route::delete('/delete/admin/data-kategori-buku/{id_kategori_buku}',['uses'=>'BukuController@DeleteKategoriBuku','as'=>'delete-data-buku']);
		// END CRUD KATEGORI BUKU //
	});
//----------End Admin----------//
});